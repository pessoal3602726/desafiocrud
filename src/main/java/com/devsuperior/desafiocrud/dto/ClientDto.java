package com.devsuperior.desafiocrud.dto;

import java.time.LocalDate;

import org.hibernate.validator.constraints.UniqueElements;

import com.devsuperior.desafiocrud.entities.Client;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.PastOrPresent;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.PositiveOrZero;
import jakarta.validation.constraints.Size;

public class ClientDto {

	private Long id;
	@NotBlank(message = "Não pode ser vazio, campo obrigatório.")
	@Size(min = 3, max = 80, message = "Nome precisa ter de 3 a 80 caracteres")
	private String name;
	@NotBlank(message = "Não pode ser vazio, campo obrigatório")
	@Size(min = 11, max = 11, message = "CPF precisa ter 11 números (sem ponto e/ou caracteres especiais)")
	private String cpf;
	@Positive
	private double income;
	@PastOrPresent
	private LocalDate birthDate;
	@PositiveOrZero
	private Integer children;

	public ClientDto(Long id, String name, String cpf, double income, LocalDate birthDate, Integer children) {
		this.id = id;
		this.name = name;
		this.cpf = cpf;
		this.income = income;
		this.birthDate = birthDate;
		this.children = children;
	}

	public ClientDto(Client entity) {
		id = entity.getId();
		name = entity.getName();
		cpf = entity.getCpf();
		income = entity.getIncome();
		birthDate = entity.getBirthDate();
		children = entity.getChildren();
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	
	public String getCpf() {
		return cpf;
	}

	public double getIncome() {
		return income;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public Integer getChildren() {
		return children;
	}

}
