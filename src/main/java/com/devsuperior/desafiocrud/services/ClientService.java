package com.devsuperior.desafiocrud.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.devsuperior.desafiocrud.dto.ClientDto;
import com.devsuperior.desafiocrud.entities.Client;
import com.devsuperior.desafiocrud.repositories.ClienteRepository;
import com.devsuperior.desafiocrud.services.exceptions.DatabaseException;
import com.devsuperior.desafiocrud.services.exceptions.ResourceNotFoundException;

import jakarta.persistence.EntityNotFoundException;

@Service
public class ClientService {
	
	@Autowired
	private ClienteRepository repository;
	
	@Transactional(readOnly = true)
	public Page<ClientDto> findAll(Pageable pageable) {
		Page<Client> clients = repository.findAll(pageable);
		return clients.map(x -> new ClientDto(x));
	}

	@Transactional(readOnly = true)
	public ClientDto findById(Long id) {
		 Client client = repository.findById(id)
				 .orElseThrow(() -> new ResourceNotFoundException("Cliente inexistente para pesquisa."));
			return new ClientDto(client);
	}
	
	@Transactional
	public ClientDto insert(ClientDto dto) {
		 Client client = new Client();
		 copyDtotoEntity(dto, client);
		 client = repository.save(client);
		 return new ClientDto(client);
	}
	
	@Transactional
	public ClientDto update(Long id, ClientDto dto) {
		try {
			Client client = repository.getReferenceById(id);
			copyDtotoEntity(dto, client);
			client = repository.save(client);
			return new ClientDto(client);
		}
		catch(EntityNotFoundException e) {
			throw new ResourceNotFoundException("Cliente inexistente para atualização.");
		}
	}
	
	@Transactional(propagation = Propagation.SUPPORTS)
	public void delete(Long id) {
		if (!repository.existsById(id)) {
			throw new ResourceNotFoundException("Cliente inexistente, não foi possível deletar.");
		}
		try {
	        	repository.deleteById(id);    		
		}
	    	catch (DataIntegrityViolationException e) {
	        	throw new DatabaseException("Falha de integridade referencial");
	   	}
	}

	private void copyDtotoEntity(ClientDto dto, Client client) {
		client.setName(dto.getName());
		client.setCpf(dto.getCpf());
		client.setBirthDate(dto.getBirthDate());
		client.setIncome(dto.getIncome());
		client.setChildren(dto.getChildren());
		
	}

	
}
